Package: spreadrate
Type: Package
Title: Estimate the spread-rate of epidemics
Version: 0.1.13
Date: 2019-11-13
Authors@R: person("Facundo", "Muñoz", email = "facundo.munoz@cirad.fr",
        role = c("aut", "cre"), comment = c(ORCID =
        "0000-0002-5061-4241"))
Description: Estimate the local velocity of propagation of an epidemic
        event, given dates and locations of observed cases. The method
        estimates the surface of first date of invasion by
        interpolation of the earliest observations in a neighbourhood
        and derives the local spread rate as the inverse slope of the
        surface. Quantify the estimation uncertainty by a Monte Carlo
        approach. Visualise and simulate the spatio-temporal progress
        of epidemics.
Depends: R (>= 3.2), sf, raster
License: GPL-3 | file LICENSE
Language: en-GB
Encoding: UTF-8
LazyData: true
URL: https://umr-astre.pages.mia.inra.fr/spreadrate
BugReports: https://forgemia.inra.fr/umr-astre/spreadrate/issues
Imports: fields, furrr, lwgeom, sp
Suggests: INLA, testthat (>= 2.1.0), ggplot2
Additional_repositories: https://inla.r-inla-download.org/R/testing
RoxygenNote: 6.1.1
